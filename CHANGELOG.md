**Licence du code source de ce fichier :** GNU General Public License v3.0 or later

Le logiciel libre `perlromathsdr` génère des dessins repérés au format `pdf`. 
Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.

Copyright (C) 2025  Jean-François Mai (alias jfm@)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

# CHANGELOG

## Main branch

## 1.0b

**20250118 :**

- Correction de bogues mineures de la version 1.0a.
- Corrections / optimisations effectuées dans le code source du fichier `./srcc/libdata.c` de la version 1.0a, notamment au niveau d'avertissements du compilateur concernant les buffers utilisés dans les directives du préprocesseur et l'utilisation de la fonction `snprintf`.

## 1.0a

**20250110 :**

- Correction de bogues mineures de la version initiale 1.0.

## 1.0

**20250107 :**

- Version initiale.
