**Licence du code source de ce fichier :** GNU General Public License v3.0 or later

Le logiciel libre `perlromathsdr` génère des dessins repérés au format `pdf`. 
Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.

Copyright (C) 2025  Jean-François Mai (alias jfm@)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

# CONTRIBUTING

Pour tout rapport de bogues ou pour toute demande d'une éventuelle amélioration ou pour toute demande d'une éventuelle évolution concernant le projet **`perlromathsdr`**, vous êtes invités à créer et à rédiger un ticket en utilisant l'instance GitLab qu'est la forge des communs numériques éducatifs du MENJS. Merci d'avance.
