# Licence du code source de ce fichier : GNU General Public License v3.0 or later

#    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
#    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
#    Copyright (C) 2025  Jean-François Mai (alias jfm@)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

MYPROG := perlromathsdr
SHELL := /bin/sh
VERSION := $(shell date +%Y%m%d-%H%M%S)
MYOS := $(shell uname)

#########################################################
## Informations qui peuvent etre modifiees (debut)     ##
## et qui se repercutent sur l'ensemble du code source ##
#########################################################

# a -> CC0 1.0
# b -> CC BY 4.0
# c -> CC BY-SA 4.0
# d -> CC BY-NC-SA 4.0
# e -> CC BY-NC 4.0
# f -> CC BY-ND 4.0
# g -> CC BY-NC-ND 4.0

AUTHOR := Jean-Fran\\\c{c}ois~Mai
# de 'a' a 'g' (voir ci-dessus)
LICENCECHOICE := a
# '3' pour cyle 3 (college) ou '4' pour cycle 4 (college)
CYCLE := 4 
# '6' pour 6eme, '5' pour 5eme, '4' pour 4eme ou '3' pour 3eme
LEVEL := 5
# Nom du dessin repere
MYPERSO := Zero

#########################################################
## Informations qui peuvent etre modifiees (fin)       ##
#########################################################

SRCC := ./srcc/
LIBC := ./libc/
OBJC := ./objc/
LOGC := ./logc/
SRCTEX := ./srctex/
LOGTEX := ./logtex/

INCLDIR := -I$(PWD) -I/usr/include

CPPFLAGS := -DVERSIONSION=\"$(VERSION)\"

CFLAGS := -Wall -Wextra -pedantic \
	 -march=native \
	 -Wmissing-prototypes \
	 -Wstrict-prototypes \
	 -Wwrite-strings \
	 $(CPPFLAGS) $(INCLDIR)

CFLAGS += -MMD
#CFLAGS += -g
#CFLAGS += -lm

ifeq ($(MYOS),OpenBSD)
	MYMAKE := gmake
	CC := cc -O2 $(CFLAGS)
	#CC := egcc -O2 $(CFLAGS)
	SED := sed -i
	RM := rm -rfP
else
ifeq ($(MYOS),NetBSD)
	MYMAKE := gmake
	CC := cc -O2 $(CFLAGS)
	SED := sed -i
	RM := rm -rfP
else
ifeq ($(MYOS),FreeBSD)
	MYMAKE := gmake
	CC := cc -O2 $(CFLAGS)
	SED := sed -i '' -e
	RM := rm -rfP
else
	MYDEBIAN := $(shell grep "debian" /etc/os-release > /dev/null && echo 1 || echo 0)
	MYARCHLINUX := $(shell grep "archlinux" /etc/os-release > /dev/null && echo 1 || echo 0)
	MYSLACKWARE := $(shell grep "slackware" /etc/os-release > /dev/null && echo 1 || echo 0)
	MYMAKE := make
	CC := gcc -O2 $(CFLAGS)
	SED := sed -i
	RM := rm -rf
endif
endif
endif

ifeq ($(MYOS),OpenBSD)
	DONE := ./mydr-$(MYOS)-$(VERSION)
else
ifeq ($(MYOS),NetBSD)
	DONE := ./mydr-$(MYOS)-$(VERSION)
else
ifeq ($(MYOS),FreeBSD)
	DONE := ./mydr-$(MYOS)-$(VERSION)
else
ifeq ($(MYOS),Linux)
ifeq ($(MYDEBIAN),1)
	DONE := ./mydr-Debian-$(VERSION)
else	
ifeq ($(MYARCHLINUX),1)
	DONE := ./mydr-ArchLinux-$(VERSION)
else
ifeq ($(MYSLACKWARE),1)
	DONE := ./mydr-SlackwareLinux-$(VERSION)
else
	DONE := ./mydr-Linux-$(VERSION)
endif
endif
endif
endif
endif
endif
endif

ifeq ($(LICENCECHOICE),a)
	LICENCEURL := https:\/\/creativecommons.org\/publicdomain\/zero\/1.0\/deed.fr
	LICENCECC := Licence Creative Commons Zero 1.0 Universal (CC0 1.0)
endif

ifeq ($(LICENCECHOICE),b)
	LICENCEURL := https:\/\/creativecommons.org\/licenses\/by\/4.0\/deed.fr
	LICENCECC := Licence Creative Commons BY 4.0 International (CC BY 4.0)
endif

ifeq ($(LICENCECHOICE),c)
	LICENCEURL := https:\/\/creativecommons.org\/licenses\/by-sa\/4.0\/deed.fr
	LICENCECC := Licence Creative Commons BY-SA 4.0 International (CC BY-SA 4.0)
endif

ifeq ($(LICENCECHOICE),d)
	LICENCEURL := https:\/\/creativecommons.org\/licenses\/by-nc-sa\/4.0\/deed.fr
	LICENCECC := Licence Creative Commons BY-NC-SA 4.0 International (CC BY-NC-SA 4.0)
endif

ifeq ($(LICENCECHOICE),e)
	LICENCEURL := https:\/\/creativecommons.org\/licenses\/by-nc\/4.0\/deed.fr
	LICENCECC := Licence Creative Commons BY-NC 4.0 International (CC BY-NC 4.0)
endif

ifeq ($(LICENCECHOICE),f)
	LICENCEURL := https:\/\/creativecommons.org\/licenses\/by-nd\/4.0\/deed.fr
	LICENCECC := Licence Creative Commons BY-ND 4.0 International (CC BY-ND 4.0)
endif

ifeq ($(LICENCECHOICE),g)
	LICENCEURL := https:\/\/creativecommons.org\/licenses\/by-nc-nd\/4.0\/deed.fr
	LICENCECC := Licence Creative Commons BY-NC-ND 4.0 International (CC BY-NC-ND 4.0)
endif

.PHONY: clean mem modes hash chash addirs deldirs

include ./mk/shell.mk
include ./mk/progs.mk
include ./mk/generic.mk
include ./mk/libs.mk
include ./mk/dirs.mk
include ./mk/pdf.mk
include ./mk/perl.mk
include ./mk/clean.mk
