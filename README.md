# Logiciel libre / open source `perlromathsdr`

---

## Licence du projet : GNU General Public License v3.0 or later

**Licence du code source de ce fichier :** GNU General Public License v3.0 or later

Le logiciel libre `perlromathsdr` génère des dessins repérés au format `pdf`. 
Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.

Copyright (C) 2025  Jean-François Mai (alias jfm@)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

---

## Outils informatiques utilisés dans le cadre du développement du projet 

Dans le cadre de ce projet ont été utilisés les outils **CLI** (**Command Line Interface**) libres / open source (et gratuits) suivants :

- Deux éditeurs de textes : vi / vim
- Un mutiplexeur de terminaux en mode texte : Tmux
- Des langages informatiques : 
    - Un moteur de production : GNU make
    - Deux langages de programmation généralistes : le langage interprété Perl et le langage compilé C
    - Un langage d'administration systèmes : Bourne shell
    - Un formateur de textes : le format LaTeX avec le moteur pdfTeX d'une distribution TeX Live
- Un logiciel pour la création des signatures cryptographiques : GNU Privacy Guard alias GnuPG ou GPG (format cryptographique `OpenPGP`)

**Remarque :** Attention de ne pas confondre la notion de logiciel libre et celle de logiciel gratuit : ne pas confondre, d'une part, la notion de **liberté logicielle** (liberté régie par, au moins, une licence logicielle, allant des licences logicielles les plus **fermées** / **restrictives** aux licences logicielles les plus **ouvertes** / **permissives**), et, d'autre part, la notion de **gratuité logicielle**. 

---

## Systèmes d'exploitation officiellement supportés par le projet

Dès le début, ce projet a été pensé pour ête portable. Aussi, ce projet a été développé sur une base de code source capable de s'adapter, de manière transparente pour les utilisatrices et les utilisateurs, aux systèmes d'exploitation de type Unix libres / open source sur lesquels celui-ci se construit, systèmes que je peux être amenés à utiliser au quotidien. 

Aussi, ce projet a été pensé, testé et rendu fonctionnel sur les systèmes d'exploitation libres / open source (et gratuits) suivants (architecture : amd64) :

- OpenBSD 7.6-stable
- FreeBSD FreeBSD 14.2-RELEASE-p0
- NetBSD 10.1-STABLE
- Debian GNU/Linux 12.9
- Arch Linux
- Slackware Linux 15.0

Pour ce qui est de systèmes d'exploitation non officiellement supportés, des techniques de **virtualisation** peuvent permettre la création d'une machine virtuelle exploitée par un système Debian GNU/Linux et ainsi permettre la construction du projet.

---

## Objectifs du code source du projet, fonctionnalités du logiciel `perlromathsdr`

L'objectif principal du projet est de construire, de manière effective et de manière la plus automatisée possible, des dessins repérés dans un repère orthonormé. 

Plus précisément, le logiciel `perlromathsdr` permet de générer (c'est ce qu'il produit en sortie donc), de manière automatique, un dessin repéré transformé (déformé ou non) d'un dessin repéré initial. Pour ce faire, il suffit de lui fournir en entrée les coordonnées du dessin repéré initial, coordonnées contenues dans des fichiers texte.

Le logiciel `perlromathsdr` est un programme totalement **CLI** (**Command Line Interface**) développé avec les langages GNU make, Bourne shell, C, Perl et LaTeX.

Le logiciel `perlromathsdr` a été conçu pour être portable (le langage C est l'un des éléments essentiels concernant la portabilité de son code source, les langages Perl et LaTeX en sont les autres éléments essentiels). Aussi, actuellement, ce programme fonctionne et se compile sur les systèmes d'exploitation mentionnés ci-dessus.

Le langage C, de par sa nature de langage compilé, de langage de programmation de bas niveau (donc proche de la machine) parmi l'ensemble des langages de haut niveau, permet de recalculer rapidement les coordonnées d'un dessin répéré initial en fonction du choix de la transformation fournie en entrée, puis de générer, à la volée, le code source LaTeX qui est ensuite compilé par la commande `latex` d'une distribution TeX Live, pour enfin donner naissance à un fichier au format `pdf` clé en main contenant la consigne et la correction du dessin repéré final. 

Le logiciel `perlromathsdr` prend en entrée :

- des coordonnées contenues dans des fichiers texte du répertoire `./data/`, coordonnées disposées dans ces fichiers selon un format prédéfini à l'avance (une base de données très simple) ;
- au début du fichier `./Makefile` (entre la ligne 38 et la ligne 46) : 
    - le prénom et le nom de l'auteur ou de l'autrice qui souhaite générer le fichier au format `pdf` du dessin repéré ;
    - la licence du fichier au format `pdf` qui sera généré (choix possibles : **CC0 1.0**, **CC BY 4.0**, **CC BY-SA 4.0**, **CC BY-NC-SA 4.0**, **CC BY-NC 4.0**, **CC BY-ND 4.0** ou **CC BY-NC-ND 4.0**) ;
    - le cycle du collège sur lequel intervient l'activité qui sera générée (choix par défaut : **4**, choix possibles : **3** ou **4**) ; 
    - le niveau du collège sur lequel intervient l'activité qui sera générée  (choix par défaut : **5**, choix possibles : **6**, **5**, **4** ou **3**) ; 
    - le nom du dessin repéré qui sera généré (choix par défaut : **Zero**) ;
- la transformation (déformation ou non) qui devra être appliquée sur les coordonnées initiales.

Le logiciel `perlromathsdr` rend alors en sortie :

- dans un nouveau répertoire `./datat/`, créé pour la circonstance de manière automatique, les nouvelles coordonnées du dessin repéré transformé (déformé ou non) contenues dans autant de fichiers texte que ceux du répertoire initial `./data/` et formatés de la même manière ;
- le code source LaTeX généré (celui lié aux nouvelles coordonnées) pour une éventuelle ré-utilisation ultérieure ;
- le fichier au format `pdf` de l'activité (contenant les informations fournies en entrée : nom, prénom, licence, cycle, niveau, nom du dessin repéré).

**Remarque :** Après l'avoir configuré comme il se doit, le logiciel `perlromathsdr` est capable de générer, de manière consécutive, plusieurs dessins repérés distincts à partir des mêmes coordonnées initiales fournies en entrée et associés à des transformations distinctes.

Une autre partie du code source, développée en langage Perl, consiste en la génération automatique des **sommes de contrôle** de l'ensemble des fichiers contenus dans l'arborescence de la racine `./` du projet, sommes associées à l'algorithme de hachage **SHA512**, sommes permettant de vérifier l'**intégrité** des données. Pour plus de cohérence et d'homogénéité pour l'ensemble du projet, le code source Perl a été intégré (et adapté pour la circonstance) dans un fichier **Makefile** dédié `./mk/perl.mk`.

Un dernier aspect du projet (aspect "caché" car n'apparait nul part dans le code source même de celui-ci) réside dans la création hors ligne (pour davantage de sécurité en lien avec la **confidentialité**) de **signatures cryptographiques** au format **`OpenPGP`** pour permettre de signer et d'**authentifier** les deux fichiers contenant les sommes de contrôle précédemment mentionnées, un premier fichier **`fingerprint.sha512.bsd.txt`** spécifique aux systèmes FreeBSD, OpenBSD et NetBSD et un second fichier **`fingerprint.sha512.linux.txt`** spécifique aux systèmes Debian GNU/Linux, Arch Linux et Slackware Linux.

---

## Crédits

Le projet a été développé de **zéro**, **from scratch** donc. 

Les **techniques informatiques**, sur lesquelles s'appuie le projet, dépendent globalement des six systèmes d'exploitation supportés, du langage C et de sa bibliothèque standard ainsi que du langage Perl (dans sa version de base, sans utilisation de modules liés au projet CPAN) et du formateur de textes LaTeX (utilisé par le biais d'une distribution TeX Live).

---

## Comment récupérer le code source du projet ?

Ouvrez un terminal puis décompressez / désarchivez le tarball de la dernière version de la branche principale du projet, tarball récupéré directement ainsi depuis la forge des communs numériques éducatifs du MENJS :

~~~sh
ada$ wget https://forge.apps.education.fr/noetheremy/perlromathsdr/-/archive/main/perlromathsdr-main.tar.gz
ada$ tar xvfzp perlromathsdr-main.tar.gz
~~~

Entrez dans le **répertoire racine** (**`./`**) du projet qui vous est désormais apparent :

~~~sh
ada$ cd perlromathsdr-main/
~~~

---

## Partie facultative néanmoins fortement conseillée : savoir vérifier l'intégrité du code source et des données numériques du projet (en lien avec la cybersécurité)

### Voici les démarches à  suivre pour vérifier les sommes de contrôle de l'ensemble des fichiers de l'arborescence du projet en se plaçant à  la racine `./` de celui-ci :

Sur un système d'exploitation Debian GNU/Linux, Arch Linux ou Slackware Linux, lancez la commande suivante  :
        
~~~sh
ada$ sha512sum -c fingerprint.sha512.linux.txt
~~~

Sur un système d'exploitation OpenBSD, lancez la commande ci-dessous :

~~~sh
ada$ sha512 -c fingerprint.sha512.bsd.txt
~~~

Sur un système d'exploitation FreeBSD, lancez celle-ci :
        
~~~sh
ada$ sha512sum -c fingerprint.sha512.bsd.txt
~~~

Sur un système d'exploitation NetBSD, effectuez ceci :
        
~~~sh
ada$ cat fingerprint.sha512.bsd.txt | cksum -c
~~~

**Remarque concernant NetBSD :** Si les vérifications effectuées ne font apparaître aucune erreur, alors rien ne vous sera rendu sur la sortie standard, à  savoir votre terminal, aucun résultat / echo donc.

À titre indicatif, sur un système FreeBSD, cette commande devrait vous rendre ceci dans votre terminal :

~~~sh
you$ sha512sum -c fingerprint.sha512.bsd.txt

./CHANGELOG.md: OK
./CONTRIBUTING.md: OK
./LICENSE: OK
./Makefile: OK
./README.md: OK
./TODO.md: OK
./VERSION.md: OK
./data/01.txt: OK
./data/02.txt: OK
./data/03.txt: OK
./data/04.txt: OK
./data/05.txt: OK
./data/06.txt: OK
./data/07.txt: OK
./img/zero_logo.png: OK
./mk/clean.mk: OK
./mk/dirs.mk: OK
./mk/generic.mk: OK
./mk/libs.mk: OK
./mk/pdf.mk: OK
./mk/perl.mk: OK
./mk/progs.mk: OK
./mk/shell.mk: OK
./srcc/libdata.c: OK
./srcc/libdata.h: OK
./srcc/libpoint.c: OK
./srcc/libpoint.h: OK
./srcc/libusage.c: OK
./srcc/libusage.h: OK
./srcc/main.c.orig: OK
./srctex/02a.tex: OK
./srctex/02b.tex.orig: OK
./srctex/licences.tex.orig: OK
./srctex/main.tex.orig: OK
./srctex/packages.tex.orig: OK
~~~

---

## Partie facultative néanmoins conseillée : savoir vérifier l'authenticité du code source et des données numériques du projet (en lien avec la cybersécurité)

### Voici les démarches à  suivre pour vérifier que les fichiers de l'arborescence du projet sont bel et bien ceux créés par la développeuse du projet : 

- Récupérez la clé cryptographique publique (au format : `ASCII` armored OpenPGP form) de la bi-clé `OpenPGP` de la développeuse, que nous nommerons Ada dans le cadre de ce projet : une référence de la clé cryptographique publique `OpenPGP` d'Ada se trouve dans l'unique **groupe** du service en ligne **apps.education.fr**, créé pour la circonstance, évoquant **`GPG`** comme mot-clé. Pour ce faire, authentifiez-vous sur le service **apps.education.fr** puis cherchez l'unique groupe utilisant le mot clé **`GPG`**. 
- Une fois trouvé, enregistrez sur votre machine le fichier texte de nom **`ada.public_key.asc`** qui contient la clé cryptographique publique `OpenPGP` d'Ada.
- Récupérez l'empreinte de cette clé cryptographique publique `OpenPGP` : celle-ci se trouve sur cette instance GitLab. Cherchez bien ! ;-)
- Ces dernières actions étant effectuées, désormais, vous allez pouvoir vérifier que le fichier **`fingerprint.sha512.bsd.txt`** (ou le fichier **`fingerprint.sha512.linux.txt`** selon l'OS que vous utilisez), contenant des sommes de contrôle de l'ensemble des fichiers de l'arborescence de la racine `./` du projet suivant l'algorithme de hachage **SHA512**, est bien **authentique**, autrement dit, que ce fichier a bel et bien été créé par Ada, développeuse du projet.
- Pour vérifier les signatures cryptographiques d'Ada, dans un premier temps, la clé publique de la développeuse doit être importée dans votre trousseau de clés `OpenPGP`. 

Pour ce faire, sur un système Debian GNU/Linux, voici la commande à utiliser :

~~~bash
you$ gpg --import ada.public_key.asc
gpg: répertoire « /home/you/.gnupg » créé
gpg: le trousseau local « /home/you/.gnupg/pubring.kbx » a été créé
gpg: /home/you/.gnupg/trustdb.gpg : base de confiance créée
gpg: clef F1E168937E725434 : clef publique « ada <ada@example.lan> » importée
gpg:       Quantité totale traitée : 1
gpg:                     importées : 1
~~~

**Remarques importantes :** 

- Sur un système Slackware Linux 15.0 cohabitent deux versions de GnuPG, la version 1 à travers la commande `gpg` et la version 2 via la commande `gpg2`. Sur ce type de système, il est donc nécessaire d'utiliser la commande `gpg2` et non la commande `gpg` (qui ne connait pas les algorithmes modernes liés aux courbes elliptiques). 
- Sur un système NetBSD 10.1-STABLE n'est installée que la version 2 de GnuPG utilisable par le biais de la commande `gpg2`. Sur ce type de système, il est donc nécessaire d'utiliser la commande `gpg2` et non la commande `gpg` (qui n'existe pas).

La vérification de l'authenticité du fichier peut alors être effectuée.

Sur un système FreeBSD, en ce qui concerne le fichier **`fingerprint.sha512.bsd.txt`**, procédez ainsi :

~~~bash
you$ gpg --verify fingerprint.sha512.bsd.txt.sign
gpg: les données signées sont supposées être dans « fingerprint.sha512.bsd.txt »
gpg: Signature faite le mar. 22 oct. 22:08:41 2025 CEST
gpg:                avec la clef EDDSA EDE26781B03C8C8CC0BA25F8A86A8C95653ED04C
gpg: Bonne signature de « ada <ada@example.lan> » [inconnu]
gpg: Attention : cette clef n'est pas certifiée avec une signature de confiance.
gpg:             Rien n'indique que la signature appartient à  son propriétaire.
Empreinte de clef principale : 4DF7 522E C2DA C8B5 4843  EEC5 F1E1 6893 7E72 5434
   Empreinte de la sous-clef : EDE2 6781 B03C 8C8C C0BA  25F8 A86A 8C95 653E D04C
~~~

À la sortie de chacune de cette commande, en bas, doit notamment apparaître une **empreinte numérique** (aussi appelée **condensat**) de la clé (principale) cryptographique publique `OpenPGP` d'Ada. Vous pouvez alors vérifier que cette empreinte correspond bien avec celle que vous avez déjà dû trouver sur cette instance GitLab du MENJS.

**Remarques :** 

- Comme les bi-clés cryptographiques (une clé privée, une clé publique) de la développeuse Ada ont été créées dans un but **pédagogique** dans le cadre de ce projet, celles-ci, même si techniquement fonctionnelles, sont **"factices"**. Il n'en reste pas moins que ces bi-clés restent, de fait, des données à caractère personnel associées à  la développeuse (fictive) Ada, des données à caractère personnel qui me sont associées en réalité. Par cette dernière remarque, un lien avec le **RGPD** est effectué ! 
- Le message **"Attention: cette clef n'est pas certifiée avec une signature de confiance."** que vous devriez rencontrer est un message normal. En effet, d'un point de vue de la cybersécurité, il nous manque une information importante concernant cette clé cryptographique publique `OpenPGP` : à qui appartient réellement cette clé, qu'est-ce qui nous **certifie** que cette clé publique est bien celle d'Ada ? Rien a priori. La situation peut se résumer ainsi : est-ce que cette clé publique est vraiment celle de la développeuse Ada ou est-ce que cette clé est celle d'une tierce personne qui essaie de se faire passer pour Ada (usurpation) ? Cette dernière question est légitime, le doute est donc permis.

---

## Comment construire l'activité au format `pdf` d'un dessin repéré lié à ce projet ?

Sur un système d'exploitation NetBSD, OpenBSD ou FreeBSD, pour obtenir le dessin repéré lié aux coordonnées **initiales**, procédez ainsi : 

~~~sh
ada$ gmake itran
~~~

Pour obtenir un **unique** dessin repéré lié aux coordonnées finales pour chacune des transformations implémentées dans le projet, faites ceci :

~~~sh
ada$ gmake utran
~~~

Pour générer des dessins repérés **distincts** construits à partir des coordonnées du dessin repéré initial, utilisez la commande suivante :

~~~sh
ada$ gmake dtran
~~~

**Remarque importante :** N'oubliez pas la lettre **g** de la commande `gmake` ci-dessus.

Sur un système d'exploitation Debian GNU/Linux, Arch Linux ou Slackware Linux, pour obtenir le dessin repéré lié aux coordonnées **initiales**, effectuez ceci :
 
~~~sh
ada$ make itran
~~~

Puis, pour obtenir un **unique** dessin repéré lié aux coordonnées finales pour chacune des transformations implémentées dans le projet, lancez la commande suivante :

~~~sh
ada$ make utran
~~~

Enfin, pour créer des dessins repérés **distincts** qui s'appuient sur les coordonnées du dessin repéré initial :

~~~bash
ada$ make dtran
~~~

Sur un système FreeBSD, une fois la compilation LaTeX terminée, un fichier au format `pdf` sera disponible à l'extérieur du répertoire racine `./` du projet, dans une archive compressée de la forme `../mydr-FreeBSD-20250107-031415.tar.gz` (signification : archive compressée générée le mardi 7 janvier 2025 à 3 h 14 min 15 s).

**Remarque :** En cas d'erreurs, veuillez prendre connaissance des informations contenues, d'une part, dans le fichier texte `./logc/erreurs.txt` (fichier lié aux erreurs de compilation associées au langage C), et, d'autre part, dans les fichiers texte `./logtex/erreurs.txt` et `./logtex/main.log` (fichiers liés à la compilation associée au langage LaTeX), les répertoires `./logc/` et `./logtex/` étant tous deux créés dès le début d'une construction et situés à la racine `./` du projet.

Pour modifier les coordonnées du dessin repéré initial, il suffit d'éditer les fichiers texte `01.txt`, `02.txt`, `03.txt`, `04.txt`,... , contenus dans le répertoire `./data/` de la racine `./` du projet. Bien évidemment, en faisant bien attention au formatage des coordonnées, il est aussi possible de créer de nouveaux fichiers de ce type dans le même répertoire.

---

## Comment utiliser directement le programme `perlromathsdr` ?

Si vous lancez la commande

~~~sh
you$ ./perlromathsdr
~~~

le logiciel vous fournira de brèves explications concernant son mode de fonctionnement.

~~~sh
you$ ./perlromathsdr

Erreur 0 : Vous devez fournir le nom d'une fonction ainsi que ses paramètres !

Merci d'utiliser le logiciel libre / open source perlromathsdr.

Ce logiciel permet d'utiliser une application du plan affine (euclidien) muni d'un repère
(orthonormé) sur lui-même qui envoie un point M de coordonnées (x ; y) en un point M' de
coordonnées (x' ; y'). Pour ce faire, vous avez le choix entre :

(1) Soit d'utiliser l'application identité 'f01Point' via la commande :

you$ ./perlromathsdr 'f01Point' 'Couleur des tracés via un nombre entier'

(2) Soit d'utiliser une application qui est un endomorphisme 'f00Point' via la commande :

you$ ./perlromathsdr 'f00Point' 'Couleur des tracés via par un entier' 'a' 'b' 'c' 'd' 'u' 'v'

en appliquant l'égalité matricielle suivante :

          / x' \     / a b \  / x \     / u \
         |      | = |       ||     | + |     |
          \ y' /     \ c d /  \ y /     \ v /

(3) Soit d'autres applications 'f02Point', 'f03Point',... 'f12Point' (que vous pourrez, si
vous le souhaitez, modifier en éditant le fichier ./srcc/libpoint.c), via la commande :

you$ ./perlromathsdr 'fnPoint' 'Couleur des tracés via un nombre entier'

avec n = 02, 03,... , 12.

Couleurs possibles :

--------------------------------------------------------------------------
1  -> BLACK | 2  -> RED      | 3  -> BLUE | 4  -> MAGENTA   | 5  -> VIOLET
6  -> GREEN | 7  -> ORANGE   | 8  -> CYAN | 9  -> YELLOW    | 10 -> LIME
11 -> BROWN | 12 -> DARKGRAY | 13 -> GRAY | 14 -> LIGHTGRAY | 15 -> OLIVE
16 -> PINK  | 17 -> PURPLE   | 18 -> TEAL
--------------------------------------------------------------------------
~~~

---

## Comment nettoyer le répertoire racine `./` de ce projet ?

Sur un système d'exploitation NetBSD, OpenBSD ou FreeBSD, lancez la commande : 

~~~sh
ada$ gmake clean
~~~

Sur un système d'exploitation Debian GNU/Linux, Arch Linux ou Slackware Linux, lancez la commande :
 
~~~sh
ada$ make clean
~~~

---

## Est-ce que le logiciel `perlromathsdr` est conforme au RGPD ?

Le code source initial du logiciel `perlromathsdr` ne comporte pas de données à caractère personnel (mis à part celles liées à son auteur). De plus, ce logiciel de type **CLI** est conçu pour être utilisable sans accès à un quelconque réseau informatique tel Internet (il ne s'agit pas d'un logiciel réseau, il n'utilise donc pas la primitive `socket()` pour faire communiquer des processus entre eux via un réseau), le logiciel `perlromathsdr` n'émet ni ne réceptionne de données de l'extérieur, n'effectue donc pas de traitements de données à caractère personnel. Ce n'est ni dans ses objectifs ni dans ses fonctionnalités d'agir ainsi. 

Aussi, en l'état, de par sa conception, le logiciel `perlromathsdr` est conforme au RGPD.

**Remarque :** Comme mentionné ci-dessus, le code source **initial** du logiciel `perlromathsdr` ne manipule pas de données (des coordonnées) à caractère personnel, mais uniquement les coordonnées du dessin repéré nommé **Zero**, logo du projet qui représente le nombre **0**. Néanmoins, dans le cadre de l'utilisation de ce logiciel, pour rester bien conforme au RGPD, en cas de diffusion sur un réseau informatique de fichiers au format `pdf` générés par `perlromathsdr` (notamment via le réseau des réseaux Internet, sur un ENT par exemple), l'utilisateur ou l'utilisatrice devra bien veiller à ce que les coordonnées fournies en entrée ne **représentent** pas **des données à caractère personnnel**, comme un nom suivi d'un prénom écrits en lettres capitales dans le repère, un numéro de téléphone d'une personne précise, une adresse IPv4 publique d'une machine liée explicitement à une personne précise,...

---

## Est-ce que le logiciel `perlromathsdr` est stable et sécurisé ?

Comme tout logiciel, une bogue peut intervenir en raison d'une situation très particulière et non prévue. Dans un cadre **normal** d'utilisation sur l'un des six systèmes d'exploitation officiellement supportés par le projet, le logiciel `perlromathsdr`  devrait se comporter comme attendu, sans risques a priori (exemple concret qui ne devrait pas se produire : sans destructions non souhaitées et définitives de données extérieures à la racine `./` du projet et situées sur le même disque que la racine `./` du projet), néanmoins, le logiciel n'est probablement pas exempt de bogues. 

Dans le cadre de son développement spécifique sur un système OpenBSD, les appels système (protection pro-active) `pledge` et `unveil` de cet OS sont explicitement utilisés dans le code source C de `perlromaths`, dans le fichier `./srcc/main.c` : cependant, ces appels système (syscall en anglais) spécifiques à un système OpenBSD ne fonctionnent par conséquent que pour OpenBSD et ne concernent pas, a priori, les cinq autres systèmes.

En outre, le logiciel `perlromathsdr` a été développé avec l'aide du logiciel libre / open source `Valgrind`. Lors des multiples tests effectués avec `Valgrind`, le logiciel a été rendu stable et exempt de fuites mémoire.

En effet, en lançant la commande ci-dessus à la racine `./` du projet, voici ce que rend le logiciel `Valgrind` sur un système FreeBSD installé avec l'ensemble des options de sécurité :

~~~sh
ada$ gmake mem
--30184:0:    main Valgrind: FATAL:
--30184:0:    main security.bsd.unprivileged_proc_debug sysctl is 0.
--30184:0:    main    Set this sysctl with
--30184:0:    main    'sysctl security.bsd.unprivileged_proc_debug=1'.
--30184:0:    main    Cannot continue.
gmake: *** [mk/shell.mk:262: mem] Error 1
~~~

Comme indiqué ci-dessus, sur un système FreeBSD muni d'options de sécurité supplémentaires, `Valgrind` nous invite à désactiver (temporairement) l'option `proc_debug` qui empêche son utilisation. En utilisant le compte administrateur `root`, cette option est donc désactivée ainsi :

~~~sh
root# sysctl security.bsd.unprivileged_proc_debug=1
security.bsd.unprivileged_proc_debug: 0 -> 1
~~~

Puis, en tant que simple utilisateur, nous lançons la commande ci-dessous qui nous indique l'absence de fuites mémoire.

~~~sh
ada$ gmake mem
==62434== Memcheck, a memory error detector
==62434== Copyright (C) 2002-2025, and GNU GPL'd, by Julian Seward et al.
==62434== Using Valgrind-3.23.0 and LibVEX; rerun with -h for copyright info
==62434== Command: ./perlromathsdr f00Point 5 .5 0 0 .5 0 0
==62434==
==62434==
==62434== HEAP SUMMARY:
==62434==     in use at exit: 0 bytes in 0 blocks
==62434==   total heap usage: 186 allocs, 186 frees, 1,189,232 bytes allocated
==62434==
==62434== All heap blocks were freed -- no leaks are possible
==62434==
==62434== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
~~~

Enfin, en tant que super-utilisateur `root`, nous réactivons alors l'option de sécurité `proc_debug` que nous venons de désactiver temporairement pour pouvoir utiliser le logiciel `Valgrind` :

~~~sh
root# sysctl security.bsd.unprivileged_proc_debug=0
security.bsd.unprivileged_proc_debug: 1 -> 0
~~~

Si l'utilisatrice ou l'utilisateur le souhaite, le logiciel `perlromathsdr` étant un logiciel libre / open source, le code source peut être lu pour vérifier si celui-ci comporte des fonctionnalités dangereuses sur l'un des systèmes d'exploitation cibles. Le cas échéant, en cas de bogue avérée, un ticket sur cette instance de GitLab qui abrite son code source pourra être créé et rédigé. 

Afin de tester le logiciel `perlromathsdr` dans un environnement sécurisé, comme déjà mentionné précédemment, une machine virtuelle exploitée par un système Debian GNU/Linux peut être utilisée.

**Remarque importante :** Quoi qu'il en soit, le logiciel `perlromathsdr` doit être compilé / utilisé avec les droits informatiques d'un simple utilisateur de l'un des six systèmes supportés et **non en tant que super-utilisateur `root`** !

---

## Quelle est la licence du fichier au format `pdf` généré par le logiciel `perlromathsdr` ?

Le choix de la licence du fichier au format `pdf` généré par le logiciel est laissé à la discrétion de la personne qui l'utilise. Nativement, au sein même de son code source, pour générer un fichier au format `pdf` de dessin repéré, le logiciel `perlromathsdr` propose les choix suivants :

- Licence Creative Commons Zero 1.0 Universal (CC0 1.0)
- Licence Creative Commons BY 4.0 International (CC BY 4.0)
- Licence Creative Commons BY-SA 4.0 International (CC BY-SA 4.0)
- Licence Creative Commons BY-NC-SA 4.0 International (CC BY-NC-SA 4.0)
- Licence Creative Commons BY-NC 4.0 International (CC BY-NC 4.0)
- Licence Creative Commons BY-ND 4.0 International (CC BY-ND 4.0)
- Licence Creative Commons BY-NC-ND 4.0 International (CC BY-NC-ND 4.0)

Il est à noter que, tout en respectant la licence GNU GPLv3+ du logiciel `perlromathsdr`, ces choix de licences peuvent être changées en modifiant son code source.
