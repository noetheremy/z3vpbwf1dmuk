**Licence du code source de ce fichier :** GNU General Public License v3.0 or later

Le logiciel libre `perlromathsdr` génère des dessins repérés au format `pdf`. 
Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.

Copyright (C) 2025  Jean-François Mai (alias jfm@)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

# TODO

- Utiliser une énumération pour la gestion des couleurs.
- Pour accélérer davantage la partie liée au langage C, créer une structure (en lien avec les pointeurs) pour les 9 paramètres liés aux transformations affines (qui seront ainsi référencés par une adresse mémoire limitant ainsi la perte de temps lors de 9 copies).
- Passer aux fonctions de hachage internes du langage Perl (raison : assurer une plus grande portabilité du code source, pour ne plus faire appel à la commande `system` du langage Perl, commande plus lourde d'utilisation faisant appel à un shell, commande qui, par ailleurs, peut poser des problèmes de portabilité et de sécurité).
- Remplacer l'outil `sed` partout il intervient dans le code source actuel par des REGEX du langage Perl (raison : toujours assurer une plus grande portabilité du code source, et, cesser de faire la distinction entre les différentes implémentations `sed` selon le système d'exploitation utilisé).
- Revoir la "politique" concernant le nom des variables, des fonctions,... : choisir entre le français ou l'anglais (ne plus mélanger les deux).
