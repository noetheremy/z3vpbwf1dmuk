# Licence du code source de ce fichier : GNU General Public License v3.0 or later

#    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
#    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
#    Copyright (C) 2025  Jean-François Mai (alias jfm@)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

adddir:
	@(install -d -m 0700 $(LOGC) && \
		install -d -m 0700 $(LIBC) && \
		install -d -m 0700 $(OBJC) && \
		install -d -m 0700 $(LOGTEX))

deldir:
	@($(RM) $(LOGC) && \
		$(RM) $(LIBC) && \
		$(RM) $(OBJC) && \
		$(RM) $(LOGTEX))
