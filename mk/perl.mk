# Licence du code source de ce fichier : GNU General Public License v3.0 or later

#    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
#    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
#    Copyright (C) 2025  Jean-François Mai (alias jfm@)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

FINGERPRINT := \
use strict; \
use warnings; \
my $$os = $$^O; \
my $$tmp = qq{./tmp.txt}; \
my @tab = qw{}; \
sub hashFingerprint() { \
	if ( $$os eq q{freebsd} or $$os eq q{netbsd} or $$os eq q{openbsd} ) { \
		system(qq{find . -type f -exec sha512 {} \\; > ./fingerprint.sha512.bsd.txt}); \
	} else { \
		system(qq{find . -type f -exec sha512sum {} \\; > ./fingerprint.sha512.linux.txt}); \
	} \
} \
sub parserFingerprint { \
	my ($$in, $$out) = @_; \
	my $$patternbsd = qr{^SHA512 (\(.\/.git\/|\(.\/fingerprint.|\(.\/log|\(.\/libc\/|\(.\/objc\/|\(.\/srcc\/main.c\)|\(.\/srctex\/packages.tex\)|\(.\/srctex\/01.tex\)|\(.\/srctex\/02b.tex\)|\(.\/srctex\/perso.tex\)|\(.\/srctex\/main.tex\)|\(.\/perlromathsdr|\(.\/datat\/|\(.\/srctex\/licences.tex\))}; \
	my $$patternlinux = qr{(.\/.git|bsd.txt|linux.txt|txt.sig|datat\/[0-9]{2}.txt|perlromathsdr.a|erreurs.txt|standard.txt|main.log|libdata.d|libdata.o|libpoint.d|libpoint.o|libusage.d|libusage.o|main.d|main.o|main.c|01.tex|02b.tex|perso.tex|main.tex|packages.tex|perlromathsdr)$$}; \
	open(IN, qq{< $$in}) || die qq{Impossible de lire le fichier $$in.\n}; \
	open(OUT, qq{> $$out}) || die qq{Impossible d ouvrir et d ecrire dans le fichier $$out.\n}; \
	while (<IN>) { \
		if ( m{$$patternbsd} or m{$$patternlinux} ) \
		{} else { print OUT; }	\
	} \
	close(IN); \
	close(OUT); \
	unlink($$in) || die qq{Impossible de supprimer le fichier $$in.\n}; \
	rename($$out, $$in) || die qq{Impossible de renommer le fichier $$out en $$in.\n}; \
	chmod(0600, $$in) || die qq{Impossible de changer les droits du fichier $$in.\n}; \
} \
sub sortFingerprint() { \
	my ($$data, $$ref_tab) = @_; \
	open(IN, qq{< $$data}) || die qq{Impossible de lire le fichier $$data.\n}; \
	while (<IN>) { \
		push(@{$$ref_tab}, $$_); \
	} \
	@{$$ref_tab} = sort(@{$$ref_tab}); \
	close(IN); \
	open(OUT, qq{> $$data}) || die qq{Impossible d ouvrir et d ecrire dans le fichier $$data.\n}; \
	foreach (@{$$ref_tab}) { print OUT $$_; } \
	close(OUT); \
	@{$$ref_tab} = qw{}; \
} \
&main::hashFingerprint(); \
if ( $$os eq q{freebsd} or $$os eq q{netbsd} or $$os eq q{openbsd} ) { \
	&main::parserFingerprint(qq{./fingerprint.sha512.bsd.txt}, $$tmp); \
	&main::sortFingerprint(qq{./fingerprint.sha512.bsd.txt}, \@tab); \
} else { \
	&main::parserFingerprint(qq{./fingerprint.sha512.linux.txt}, $$tmp); \
	&main::sortFingerprint(qq{./fingerprint.sha512.linux.txt}, \@tab); \
}

hash:
	@perl -e '$(FINGERPRINT)'

chash:
	@$(MYMAKE) clean 
	@$(MYMAKE) hash
