# Licence du code source de ce fichier : GNU General Public License v3.0 or later

#    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
#    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
#    Copyright (C) 2025  Jean-François Mai (alias jfm@)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

progc:
	@$(MYMAKE) modes
	@$(MYMAKE) adddir
	@$(MYMAKE) libstatic \
		2> $(LOGC)/erreurs.txt \
		| tee $(LOGC)/standard.txt
	@${CC} ${OBJC}main.o \
		$(LIBC)lib$(MYPROG).a \
		-o $(MYPROG) \
		2>> $(LOGC)/erreurs.txt \
		| tee $(LOGC)/standard.txt
	@ldd $(MYPROG) \
		2>> $(LOGC)/erreurs.txt \
		| tee $(LOGC)/standard.txt

progtex:
	@$(SED) 's/\.0//g;s/\./,/g' ./01.tex
	@iconv -f utf-8 -t iso8859-1 ./01.tex > $(SRCTEX)/01.tex
	@iconv -f utf-8 -t iso8859-1 ./perso.tex > $(SRCTEX)/perso.tex
	@${RM} ./01.tex ./perso.tex
	@cp -f $(SRCTEX)/02b.tex.orig $(SRCTEX)/02b.tex
	@$(SED) "s/QUI/${MYPERSO}/" $(SRCTEX)/02b.tex

	@cp -f $(SRCTEX)/main.tex.orig $(SRCTEX)/main.tex
	@cp -f $(SRCTEX)/packages.tex.orig $(SRCTEX)/packages.tex
	@cp -f $(SRCTEX)/licences.tex.orig $(SRCTEX)/licences.tex

ifeq ($(MYOS),OpenBSD)
	@$(SED) 's/QUELOS/OpenBSD/' $(SRCTEX)/main.tex
else
ifeq ($(MYOS),NetBSD)
	## /!\ ##
	# L'extension 'pst-arrow.sty' semble ne pas exister pas dans la version 
	# Tex Live 2025 de NetBSD 10.0-STABLE
	# Une solution : On recupere les sources manquantes sur le site Web officiel 
	# du projet CTAN, et, en nous aidant de la commande 'kpsewhich pst-arrow' qui nous 
	# rend le chemin sur un systeme FreeBSD 14.1-RELEASE-p5 : 
	# /usr/local/share/texmf-dist/tex/latex/pst-arrow/pst-arrow.sty
	# on installe le necessaire dans le repertoire 'texmf/' de l'utilisatrice Ada,
	# dans '~/texmf/tex/latex/'. Voila !
	# URL : https://ctan.org/pkg/pst-arrow
	@$(SED) 's/QUELOS/NetBSD/' $(SRCTEX)/main.tex
	## /!\ ##
else
ifeq ($(MYOS),FreeBSD)
	@$(SED) 's/QUELOS/FreeBSD/' $(SRCTEX)/main.tex
else
ifeq ($(MYOS),Linux)
ifeq ($(MYDEBIAN),1)
	@$(SED) 's/QUELOS/Debian GNU\/Linux/' $(SRCTEX)/main.tex
else	
ifeq ($(MYARCHLINUX),1)
	@$(SED) 's/QUELOS/Arch Linux/' $(SRCTEX)/main.tex
else
ifeq ($(MYSLACKWARE),1)
	@cp -f $(SRCTEX)/packages.tex.orig $(SRCTEX)/packages.tex
	## /!\ ##
	# Le package '\usepackage[upright]{fourier}' semble ne pas exister pas dans 
	# la version Tex Live 2023 de la Slackware Linux 15.0, aussi, faute de meilleur
	# on le desactive sur cet OS, pour l'instant du moins !
	@$(SED) 's/\\usepackage\[upright\]{fourier}//g' $(SRCTEX)/packages.tex
	@$(SED) 's/QUELOS/Slackware Linux/' $(SRCTEX)/main.tex
	## /!\ ##
else
	@$(SED) 's/QUELOS/Linux/' $(SRCTEX)/main.tex
endif
endif
endif
endif
endif
endif
endif

	@$(SED) "s/AUTHOR/$(AUTHOR)/" $(SRCTEX)/main.tex
	@$(SED) "s/LICENCEURL/$(LICENCEURL)/" $(SRCTEX)/main.tex
	@$(SED) "s/LICENCECC/$(LICENCECC)/" $(SRCTEX)/main.tex
	@${SED} "s/LEVEL/$(LEVEL)/" $(SRCTEX)/main.tex
	@${SED} "s/CYCLE/$(CYCLE)/" $(SRCTEX)/main.tex
	
	@$(SED) "s/AUTHOR/${AUTHOR}/" $(SRCTEX)/licences.tex
	@$(SED) "s/LICENCECC/$(LICENCECC)/" $(SRCTEX)/licences.tex
	@$(SED) "s/LEVEL/${LEVEL}/" $(SRCTEX)/licences.tex
	@${SED} "s/CYCLE/$(CYCLE)/" $(SRCTEX)/licences.tex
	@${SED} "s/MYPERSO/$(MYPERSO)/" $(SRCTEX)/licences.tex
	
	@$(MYMAKE) all \
		2> $(LOGTEX)/erreurs.txt \
		| tee $(LOGTEX)/standard.txt
