# Licence du code source de ce fichier : GNU General Public License v3.0 or later

#    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
#    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
#    Copyright (C) 2025  Jean-François Mai (alias jfm@)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# On (re-)initialise les droits sur les fichiers / repertoires de la racine du projet

MODEINIT700 := $(shell chmod 0700 ./data/ \
        ./mk/ \
        ./img/ \
        ./srcc/ \
        ./srctex/ )

MODEINIT600 := $(shell chmod 0600 ./CHANGELOG.md \
	./CONTRIBUTING.md \
	./Makefile \
	./README.md \
	./TODO.md \
	./VERSION.md \
	./fingerprint.*.txt \
	./mk/* \
	./img/* \
	./srcc/* \
	./data/* \
	./srctex/*)

MODEINIT400 := $(shell chmod 0400 ./fingerprint.*.txt.sig \
	./LICENSE )

VALGRIND := valgrind --tool=memcheck \
	--leak-check=full \
	--leak-resolution=high \
	--track-origins=yes \
	--show-reachable=yes \
	-s ./$(MYPROG) 'f00Point' '5' '-0.5' '0' '0' '.5' '0' '0'

MYPWD := $(shell pwd | sed 's/\//\\\//g') 

COLOR01 := '1' # BLACK
COLOR02 := '2' # RED
COLOR03 := '3' # BLUE
COLOR04 := '4' # MAGENTA
COLOR05 := '5' # VIOLET
COLOR06 := '6' # GREEN
COLOR07 := '7' # ORANGE
COLOR08 := '8' # CYAN
COLOR09 := '9' # YELLOW
COLOR10 := '10' # LIME
COLOR11 := '11' # BROWN
COLOR12 := '12' # DARKGRAY
COLOR13 := '13' # GRAY
COLOR14 := '14' # LIGHTGRAY
COLOR15 := '15' # OLIVE
COLOR16 := '16' # PINK
COLOR17 := '17' # PURPLE
COLOR18 := '18' # TEAL

# Information : De maniere abusive d'un point de vue de la theorie mathematique,
# sera employee l'expression "transformation (affine)" pour une application 
# (affine) du plan (identifie a |Rx|R) dans lui-meme qui ne le sera pas necessairement. 
# Raison : Simplifier les commentaires dans le code source. 

# application de la transformation identitee dans un repere

itran:
	@$(MYMAKE) clean
	@cp -f $(SRCC)/main.c.orig $(SRCC)/main.c
	@$(SED) "s/QUI/$(MYPWD)/" $(SRCC)/main.c
	@$(SED) 's/ \";/\";/g' $(SRCC)/main.c
	@$(MYMAKE) progc
	@$(MYMAKE) hash
	# Avant : application identite (f01point) en couleur magenta (4) : 
	# construit le dessin repere tel que fourni par ses coordonnees initiales
	@./$(MYPROG)  'f01Point' $(COLOR04)
	@mkdir $(DONE)
	@($(MYMAKE) progtex && $(MYMAKE) all)
	@mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)00.pdf
	@mv ./datat $(DONE)/datat00
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat00/
	@tar cvfzp $(DONE).tar.gz $(DONE)
	@chmod 0400 $(DONE).tar.gz
	@mv $(DONE).tar.gz ..
	@$(RM) $(DONE)

# l'ensemble des transformations dans un meme repere unique

utran:
	@$(MYMAKE) clean
	@cp -f $(SRCC)/main.c.orig $(SRCC)/main.c
	@$(SED) "s/QUI/$(MYPWD)/" $(SRCC)/main.c
	@$(SED) 's/ \";/\";/g' $(SRCC)/main.c
	@$(MYMAKE) progc
	@$(MYMAKE) hash
	@mkdir $(DONE)
	## /!\ Changer ici /!\ (debut) ##
	# Apres :
	## Coordonnees d'origine : (debut) ##
	@./$(MYPROG) 'f01Point' $(COLOR11)
	@echo "Phase 1 done !"
	@mv ./datat $(DONE)/datat00
	## Coordonnees d'origine : (fin) ##
	## Translation de vecteur (4 ; -5) : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR01) '1' '0' '0' '1' '4' '-5'
	@echo "Phase 2 done !"
	@mv ./datat $(DONE)/datat01
	## Translation de vecteur (4 ; -5) : (fin) ##
	## Homothetie de centre O et de rapport 1/2 : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR02) '.5' '0' '0' '.5' '0' '0'
	@mv ./datat $(DONE)/datat02
	@echo "Phase 3 done !"
	## Homothetie de centre O et de rapport 1/2 : (debut) ##
	## Symetrie centrale de centre O : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR03) '-1' '0' '0' '-1' '0' '0'
	@mv ./datat $(DONE)/datat03
	@echo "Phase 4 done !"
	## Symetrie centrale de centre O : (fin) ##
	## Symetrie axiale d'axe l'axe des ordonnees : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR04) '-1' '0' '0' '1' '0' '0'
	@mv ./datat $(DONE)/datat04
	@echo "Phase 5 done !"
	## Symetrie axiale d'axe l'axe des ordonnees : (fin) ##
	## Symetrie axiale d'axe 'vertical'  passant par le point (-2;0) : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR05) '-1' '0' '0' '1' '-4' '0'
	@mv ./datat $(DONE)/datat05
	@echo "Phase 6 done !"
	## Symetrie axiale d'axe 'vertical'  passant par le point (-2;0) : (fin) ##
	## Symetrie axiale d'axe l'axe des abscisses : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR06) '1' '0' '0' '-1' '0' '0'
	@mv ./datat $(DONE)/datat06
	@echo "Phase 7 done !"
	## Symetrie axiale d'axe l'axe des abscisses : (fin) ##
	## Symetrie axiale d'axe 'horizontal'  passant par le point (0;9) : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR07) '1' '0' '0' '-1' '0' '18'
	@mv ./datat $(DONE)/datat07
	@echo "Phase 8 done !"
	## Symetrie axiale d'axe 'horizontal'  passant par le point (0;9) : (fin) ##
	## Dilatation de base l'axe des ordonnees, de direction l'axe des abscisses et de rapport 1/4 : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR08) '.25' '0' '0' '1' '0' '0'
	@mv ./datat $(DONE)/datat08
	@echo "Phase 9 done !"
	## Dilatation de base l'axe des ordonnees, de direction l'axe des abscisses et de rapport 1/4 : (fin) ##
	## Dilatation de base l'axe des abscisses, de direction l'axe des ordonnees et de rapport -1/2 : (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR09) '1' '0' '0' '-.5' '0' '0'
	@mv ./datat $(DONE)/datat09
	@echo "Phase 10 done !"
	## Dilatation de base l'axe des abscisses, de direction l'axe des ordonnees et de rapport -1/2 : (fin) ##
	@./$(MYPROG) 'f00Point' $(COLOR10) \
	       '1.2*cos(M_PI/4)' '1.2*sin(M_PI/4)' '-1.2*sin(M_PI/4)' '1.2*cos(M_PI/4)' \
	       '-8.5' '-25'
	@mv ./datat $(DONE)/datat10
	@echo "Phase 11 done !"
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all)
	@mv $(MYPERSO).pdf $(DONE)/$(MYPERSO).pdf
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/
	@tar cvfzp $(DONE).tar.gz $(DONE)
	@chmod 0400 $(DONE).tar.gz
	@mv $(DONE).tar.gz ..
	@$(RM) $(DONE)

# l'ensemble des transformations dans des reperes distincts, avec
# autant de reperes que de transformations

dtran:
	@$(MYMAKE) clean
	@cp -f $(SRCC)/main.c.orig $(SRCC)/main.c
	@$(SED) "s/QUI/$(MYPWD)/" $(SRCC)/main.c
	@$(SED) 's/ \";/\";/g' $(SRCC)/main.c
	@$(MYMAKE) progc
	@$(MYMAKE) hash
	@mkdir $(DONE)
	## T01 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f01Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)00.pdf && mv ./datat $(DONE)/datat00)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat00/
	## T02 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f00Point' $(COLOR05) \
		'1.2*cos(M_PI/4)' '1.2*sin(M_PI/4)' '-1.2*sin(M_PI/4)' '1.2*cos(M_PI/4)' \
		'-8.5' '-25'
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)01.pdf && mv ./datat $(DONE)/datat01)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat01/
	## T03 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f02Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)02.pdf && mv ./datat $(DONE)/datat02)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat02/
	## T04 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f03Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)03.pdf && mv ./datat $(DONE)/datat03)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat03/
	## T05 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f04Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)04.pdf && mv ./datat $(DONE)/datat04)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat04/
	## T06 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f05Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)05.pdf && mv ./datat $(DONE)/datat05)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat05/
	## T07 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f06Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)06.pdf && mv ./datat $(DONE)/datat06)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat06/
	## T08 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f07Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)07.pdf && mv ./datat $(DONE)/datat07)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat07/
	## T09 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f08Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)08.pdf && mv ./datat $(DONE)/datat08)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat08/
	## T10 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f09Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)09.pdf && mv ./datat $(DONE)/datat09)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat09/
	## T11 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f10Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)10.pdf && mv ./datat $(DONE)/datat10)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat10/
	## T12 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f11Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)11.pdf && mv ./datat $(DONE)/datat11)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat11/
	## T13 -> /!\ Changer ici /!\ (debut) ##
	@./$(MYPROG) 'f12Point' $(COLOR05)
	## /!\ Changer ici /!\ (fin) ##
	@($(MYMAKE) progtex && $(MYMAKE) all && mv $(MYPERSO).pdf $(DONE)/$(MYPERSO)12.pdf && mv ./datat $(DONE)/datat12)
	@mv $(SRCTEX)/01.tex $(SRCTEX)/perso.tex $(DONE)/datat12/
	@tar cvfzp $(DONE).tar.gz $(DONE)
	@chmod 0400 $(DONE).tar.gz
	@mv $(DONE).tar.gz ..
	@$(RM) $(DONE)

modes:
	@$(MODEINIT0700)
	@$(MODEINIT0600)
	@$(MODEINIT0400)

mem:
	@$(VALGRIND)
