/*
* Licence du code source de ce fichier : GNU General Public License v3.0 or later

*    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
*    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
*    Copyright (C) 2025  Jean-François Mai (alias jfm@)
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.

*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.

*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <dirent.h> // compter le nombre de fichiers dans un répertoire
#include <err.h>
#include <errno.h> // errno
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // errno -> strerror
#include <sys/stat.h> // mkdir
#include <sys/types.h> // mkdir
#include <unistd.h> // primitive write

#include "./srcc/libpoint.h"
#include "./srcc/libpoint.c"
#include "./srcc/libdata.h"

#define MAX_ETAPES 30 // nombre maximal d'etapes autorisees
#define TAILLE_NOM_FICHIER 16 // 14+1 pour tenir compte du \0 + 1 = 16 pour NetBSD
// tableau de char a 3 element : element 1 et 2 = numero etape avec deux chiffres / element 3 = \0
#define NOMBRE_NOM_FICHIER 4 // 2+1 pour \0 + 1 = 4 pour NetBSD
#define BUFFER1 16
#define BUFFER2 4
#define FICHIER_DATA "./data/" // coordonnees initiales
#define FICHIER_DATAT "./datat/" // coordonnes finales
#define BLACK fprintf (dst2, "\\psline[linewidth=2pt,linecolor=black]"); 
#define RED fprintf (dst2, "\\psline[linewidth=2pt,linecolor=red]");
#define BLUE fprintf (dst2, "\\psline[linewidth=2pt,linecolor=blue]");
#define MAGENTA fprintf (dst2, "\\psline[linewidth=2pt,linecolor=magenta]");
#define VIOLET fprintf (dst2, "\\psline[linewidth=2pt,linecolor=violet]");
#define GREEN fprintf (dst2, "\\psline[linewidth=2pt,linecolor=green]");
#define ORANGE fprintf (dst2, "\\psline[linewidth=2pt,linecolor=orange]");
#define CYAN fprintf (dst2, "\\psline[linewidth=2pt,linecolor=cyan]");
#define YELLOW fprintf (dst2, "\\psline[linewidth=2pt,linecolor=yellow]");
#define LIME fprintf (dst2, "\\psline[linewidth=2pt,linecolor=lime]");
#define BROWN fprintf (dst2, "\\psline[linewidth=2pt,linecolor=brown]");
#define DARKGRAY fprintf (dst2, "\\psline[linewidth=2pt,linecolor=darkgray]");
#define GRAY fprintf (dst2, "\\psline[linewidth=2pt,linecolor=gray]");
#define LIGHTGRAY fprintf (dst2, "\\psline[linewidth=2pt,linecolor=lightgray]");
#define OLIVE fprintf (dst2, "\\psline[linewidth=2pt,linecolor=olive]");
#define PINK fprintf (dst2, "\\psline[linewidth=2pt,linecolor=pink]");
#define PURPLE fprintf (dst2, "\\psline[linewidth=2pt,linecolor=purple]");
#define TEAL fprintf (dst2, "\\psline[linewidth=2pt,linecolor=teal]");

// Construction d'une étape uniquement de ./01.tex

int // nombre total de points de l'etape concernee 
consigneTeXDebut(const char fsrc[], // ./data/??.txt
		const char fdst1[], // ./01.tex
		const char fdst2[], // ./perso.tex
		char *name, float aa, float bb,
		float cc, float dd,
		float uu, float vv, 
		int couleur,
		coordonnees (*f)(coordonnees), 
		int choix) // si 0 fonction affine sinon autre fonction
{ 
	FILE 	*src = NULL, *dst1 = NULL, *dst2 = NULL;
	char 	*ligne = NULL, *fin = NULL;
 	size_t 	longueur = 0;
 	ssize_t	nread;
	float 	a = 0, b = 0;
	// compte le nombre de points par etape, compte modulo 7
	// compte le nombre total de points pour l'etape
 	int compt = 0, total = 0; 
 	coordonnees point, tpoint;

	src = fopen(fsrc, "rt");
   	dst1 = fopen(fdst1, "at");
	dst2 = fopen(fdst2, "at");
	
	if (src == NULL) { 
		perror("Attention, souci dans consigneTeXDebut -> src !");
		exit(EXIT_FAILURE); 
	}
	
	if (dst1 == NULL) { 
		perror("Attention, souci dans consigneTeXDebut -> dst1 !");
		exit(EXIT_FAILURE); 
	}
 	
	if (dst2 == NULL) { 
		perror("Attention, souci dans consigneTeXDebut -> dst2 !");
		exit(EXIT_FAILURE); 
	}
	
	if (src != NULL) { 
		if (atoi(name) == 1) {
   			fprintf(dst1, "\\uline{\\textbf{Étape \\no %d :}}\n\n", atoi(name)); 
  		} else {
   			fprintf(dst1, "\n\n\\uline{\\textbf{Étape \\no %d :}}\n\n", atoi(name)); 
  		}
  		fprintf(dst1, "\\medskip\n\n"); 
 		fprintf(dst1, "\\textbf{DÉBUT :~}"); 

		// couleur des segments traces -> enumeration plutot ??!
		if (couleur == 1) { BLACK } 
		else if (couleur == 2) { RED } 
		else if (couleur == 3) { BLUE } 
		else if (couleur == 4) { MAGENTA } 
		else if (couleur == 5) { VIOLET } 
		else if (couleur == 6) { GREEN } 
		else if (couleur == 7) { ORANGE }	
		else if (couleur == 8) { CYAN }	
		else if (couleur == 9) { YELLOW }	
		else if (couleur == 10) { LIME }	
		else if (couleur == 11) { BROWN }	
		else if (couleur == 12) { DARKGRAY }	
		else if (couleur == 13) { GRAY }	
		else if (couleur == 14) { LIGHTGRAY }	
		else if (couleur == 15) { OLIVE }	
		else if (couleur == 16) { PINK }	
		else if (couleur == 17) { PURPLE }	
		else if (couleur == 18) { TEAL }	
 	
		while ((nread = getline (&ligne, &longueur, src)) != -1) {
			a = (float)strtod(ligne, &fin);
			b = (float)strtod(++fin, &fin);
			point = construitPoint(a, b);
			if (choix == 0) { // si 0 alors fonction affine (mettre f01 pour la fonction)
				tpoint = f00Point(point, aa, bb, cc, dd, uu, vv);
			} else { // sinon autre fonction (mettre n'importe quoi pour aa, bb,...)
				tpoint = (*f)(point);
			}
			detruitPoint(point);
			compt++;
			if ((compt % 7) != 0) { 
				fprintf(dst1, "($%.1f$~;~$%.1f$)$\\rightarrow$",
						tpoint->abscisse,
					       	tpoint->ordonnee);
  			} else {
				fprintf(dst1, "($%.1f$~;~$%.1f$)$\\rightarrow$\n\n\\medskip\n\n",
						tpoint->abscisse, 
	    					tpoint->ordonnee);
   			}	
   			fprintf(dst2, "(%.1f,%.1f)",
					tpoint->abscisse, 
		    			tpoint->ordonnee);
			detruitPoint(tpoint);
		}
  	
		total += compt; 
  		fprintf(dst1, "\\textbf{FIN}%% %d points\n\n\\medskip", compt); 
  		fclose(dst1);
  		fprintf(dst2, "%% %d points\n\n", compt); 
  		fclose(dst2);
	}

	free(ligne);
	fclose(src);
	return total; 
}

/* Construction de ./01.tex
	dst1 -> ./01.tex
	dst2 -> ./perso.tex
	idee : 
	-> si aa = 1, bb =0, cc = 0 , dd = 1, uu = 0 et vv = 0 alors cas du personnage initial
	-> sinon c'est le personnage transformé
*/

void 
consigneTeX(float aa, float bb,
		float cc, float dd,
		float uu, float vv, 
		int couleur, 
		coordonnees (*f)(coordonnees), 
		int choix) // si 0 fonction affine sinon autre fonction 
{
	DIR 	*data = opendir(FICHIER_DATA);
      	int 	nombre = 0, compt = 1;
      	char 	name[TAILLE_NOM_FICHIER] = { '\0' }; // nom d'un fichier du dessin repere initial 
	// tableau de char a 3 element : element 1 et 2 = numero etape avec deux chiffres / element 3 = \0 
	// numero[0] et numero[1] = numero avec 2 chiffres de l'etape, chiffre des dizaines pouvant etre
	// egal a 0
      	char 	numero[NOMBRE_NOM_FICHIER] = { '\0' };
	int 	total = 0; // compte le nombre total de points 
	
	if (data == NULL) { 
		perror("Attention, souci dans consigneTeX -> data !");
		exit(EXIT_FAILURE); 
	}
      
	nombre = compterFichiers(data);
      	closedir(data);

	while (compt <= nombre) { 
		if (compt < 10) {
			snprintf((char*)&name, BUFFER1, "./data/0%d.txt", compt);
		     	snprintf((char*)&numero, BUFFER2, "0%d", compt);
	      	} else {
			snprintf((char*)&name, BUFFER1, "./data/%d.txt", compt);
		      	snprintf((char*)&numero, BUFFER2, "%d", compt);
	      	}
		total += consigneTeXDebut(name, // un fichier etapes de ./data/??.txt 
				"./01.tex", // ./01.tex
				"./perso.tex", // ./perso.tex
			      	numero, aa, bb, cc, dd, uu, vv, 
				couleur, f, choix);
	      	compt++;
		if (compt > MAX_ETAPES) { 
			err(EXIT_FAILURE, "Attention, le nombre de fichiers du dessin repéré initial est strictement supérieur au nombre maximal autorisé. Le programme cesse de fonctionner immédiatement. Désolé !\n");
		}
      	}
      	
	consigneTeXFin("./01.tex", total); // ./01.tex + nombre total de points
}

// Construction de la fin de ./01.tex

void 
consigneTeXFin(const char fdst[], int total) 
{ 
	FILE 	*dst = NULL;

	dst = fopen(fdst, "at");

	if (dst == NULL) { 
		perror("Attention, souci dans consigneTeXFin -> dst !");
		exit(EXIT_FAILURE); 
	} else {
  		fprintf(dst, "\n\\bigskip\n\n\\uline{\\textbf{Question :}} Qui suis-je ?\\dotfill\n\n"); 
  		fprintf(dst, "%% Nombre total de points : %d !\n", total);
  		fprintf(dst, "\\bigskip\n");
 	}
 
	fclose(dst);
}
		
int 
compterFichiers(DIR* dir) 
{
	int 	compt = 0;
	struct 	dirent *ent = NULL;

	while ((ent = readdir(dir)) != NULL) {
   		if ((strcmp(ent->d_name, ".") != 0) && // Si le fichier lu n'est pas '.' 
    		strcmp(ent->d_name, "..") != 0) //  et n'est pas '..' non plus
    		compt++; // alors on incrémente le compteur
   	}
 	return compt;
	free(ent);
	ent = NULL;
}

// Construction d'un fichier ./datat/??.txt a partir du fichier initial ./data/??.txt

void
constructDataDebut(const char fsrc[], // ./data/??.txt
		const char fdst[], // ./datat/??.txt
		float aa, float bb, float cc, 
		float dd, float uu, float vv,
		coordonnees (*f)(coordonnees), 
		int choix) // si 0 fonction affine sinon autre fonction
{
	FILE 	*src = NULL, *dst = NULL;
	char 	*ligne = NULL, *fin = NULL;
 	size_t 	longueur = 0;
 	ssize_t	nread;
 	float 	a = 0, b = 0;
 	coordonnees point, tpoint;

 	src = fopen(fsrc, "rt");
	dst = fopen(fdst, "at");
	
	if (src == NULL) {
		perror("Attention, souci dans consigneDataDebut -> src !");
		exit(EXIT_FAILURE); 
	}
	
	if (dst == NULL) { 
		perror("Attention, souci dans consigneDataDebut -> dst !");
		exit(EXIT_FAILURE); 
	}

	if (src != NULL) { 
		while ((nread = getline (&ligne, &longueur, src)) != -1) {
   			a = (float)strtod(ligne, &fin);
   			b = (float)strtod(++fin, &fin);
			point = construitPoint(a, b);
			if (choix == 0) { // si 0 alors fonction affine (mettre f01 pour la fonction)
				tpoint = f00Point(point, aa, bb, cc, dd, uu, vv);
			} else { // sinon autre fonction (mettre n'importe quoi pour aa, bb,...)
				tpoint = (*f)(point);
			}
			detruitPoint(point);
    			fprintf(dst, "%.1f %.1f\n",
					tpoint->abscisse, 
					tpoint->ordonnee); 
			detruitPoint(tpoint);
 		}
	}

	fclose(dst);
	free(ligne);
	fclose(src);
}

// Construction de l'ensemble des fichiers ./datat/??.txt

void
constructData(float aa, float bb, float cc, 
		float dd, float uu, float vv,
		coordonnees (*f)(coordonnees), 
		int choix) // si 0 fonction affine sinon autre fonction 
{

	DIR 	*fd = opendir(FICHIER_DATA); // repertoire ./data/ "ouvert"
      	int 	nombre = 0,
		compt = 1;
      	char 	name[TAILLE_NOM_FICHIER] = { '\0' }, // nom du fichier de ./data/??.txt
		datat[TAILLE_NOM_FICHIER] = { '\0' }; // nom du fichier de ./datat/??.txt associe
	
	if (fd == NULL) { 
		perror("Attention, souci dans constructData -> fd !");
		exit(EXIT_FAILURE); 
	}
	
	if (BUFFER1 < strlen("./data/99.txt")) { // strlen("./data/99.txt") = 14 sans le caractere \0 !
		perror("Attention, souci dans constructData -> BUFFER1 !");
		exit(EXIT_FAILURE); 
	}

	if (BUFFER2 < strlen("99")) { // strlen("99") = 3
		perror("Attention, souci dans constructData -> BUFFER2 !");
		exit(EXIT_FAILURE); 
	}
	
	nombre = compterFichiers(fd);
      	closedir(fd);
	mkdir(FICHIER_DATAT, 0700);
	
	while (compt <= nombre) {
		if (compt < 10) {
			snprintf((char*)&name, BUFFER1, "./data/0%d.txt", compt);
			snprintf((char*)&datat, BUFFER1, "./datat/0%d.txt", compt);
	      	} else {
			snprintf((char*)&name, BUFFER1, "./data/%d.txt", compt);
			snprintf((char*)&datat, BUFFER1, "./datat/%d.txt", compt);
	      	}
		constructDataDebut(name, // ./data/??.txt
				datat, // ./datat/??.txt associe
				aa, bb, 
			      	cc, dd, 
				uu, vv, 
				f, choix); // rappel : f = &f
	      	compt++;
		if (compt > MAX_ETAPES) { 
			err(EXIT_FAILURE, "Attention, le nombre de fichiers du dessin repéré initial est strictement supérieur au nombre maximal autorisé. Le programme cesse de fonctionner immédiatement. Désolé !\n");
		}
      	}
}
