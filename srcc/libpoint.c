/*
* Licence du code source de ce fichier : GNu General Public License v3.0 or later

*    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
*    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
*    Copyright (C) 2025  Jean-François Mai (alias jfm@)
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNu General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.

*    This program is distributed in the hope that it will be useful,
*    but WITHOuT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICuLAR PuRPOSE.  See the
*    GNu General Public License for more details.

*    You should have received a copy of the GNu General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "./srcc/libpoint.h"

struct _coordonnees {
	float abscisse;
 	float ordonnee;
};

/* constructeur */

coordonnees construitPoint(float x, float y) {

	coordonnees point;

 	point = (coordonnees)malloc(sizeof(coordonnees));
	
	if (!point) {
  		printf("Erreur: plus de mémoire\n");
  		exit(EXIT_FAILURE);
 	}

	point->abscisse = x;
 	point->ordonnee = y;
	return point;
}

/* destructeur */

void detruitPoint(coordonnees point) {
	free(point);
	point = NULL;
}

/* transformations sur les coordonnées */

/* Transformation f00Point (debut) */
/* Affinites */

// Point initial :
// a = 1, b = 0, c = 0, d = 1, u = 0 et v = 0

// Point transforme :
/*
*  Rotation de centre O et d'angle de mesure theta :
*  a = cos(theta) ; b = -sin(theta) ; c = cos(theta) ; d = sin(theta) ; u = 0 ; v = 0
*  Translation de vecteur de coordonnees (e;f) :
*  a = 1 ; b = 0 ; c = 0 ; d = 1 ; u = e ; v = f
*  Homothetie de centre O et de rapport k :
*  a = k ; b = 0 ; c = 0 ; d = k ; u = 0 ; v = 0
*  Symetrie centrale de centre O :
*  a = -1 ; b = 0 ; c = 0 ; d = -1 ; u = 0 ; v = 0
*  Symétrie axiale d'axe l'axe des ordonnees :
*  a = -1 ; b = 0 ; c = 0 ; d = 1 ; u = 0 ; v = 0
*  Symetrie axiale d'axe 'vertical' passant par le point (x=aa;y=0) dans l'ancien repere :
*  a = -1 ; b = 0 ; c = 0 ; d = 1 ; u = +2aa ; v = 0
*  Symetrie axiale d'axe l'axe des abcisses :
*  a = 1 ; b = 0 ; c = 0 ; d = -1 ; u = 0 ; v = 0
*  Symetrie axiale d'axe 'horizontale' passant par le point (x=0;y=bb) dans l'ancien repere :
*  a = 1 ; b = 0 ; c = 0 ; d = -1 ; u = 0 ; v = +2bb
*  Dilatation de base l'axe des ordonnees, de direction l'axe des abscisses et de rapport k :
*  a = k ; b = 0 ; c = 0 ; d = 1 ; u = 0 ; v = 0
*  Dilatation de base l'axe des abscisses, de direction l'axe des ordonnees et de rapport k :
*  a = 1 ; b = 0 ; c = 0 ; d = k ; u = 0 ; v = 0
*/

coordonnees f00Point(coordonnees point, 
		float a, float b, 
		float c, float d,
		float u, float v) {

	coordonnees p = NULL;

 	p = construitPoint(point->abscisse*a+point->ordonnee*b+u, 
			point->abscisse*c+point->ordonnee*d+v);
 	return p;
}

/* Transformation f00Point (fin) */

/* Transformation f01Point (debut) */
/* Identitee */

coordonnees f01Point(coordonnees point) {
 	
	coordonnees p = NULL;

 	p = construitPoint(point->abscisse, 
			point->ordonnee);
 	return p;
}

/* Transformation f01Point (fin) */

coordonnees f02Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			((point->abscisse)*(point->abscisse)-(point->ordonnee)*(point->ordonnee))/50, 
			(2*(point->abscisse)*(point->ordonnee))/50);
 	return p;
}

coordonnees f03Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			(20*(point->abscisse)*(point->abscisse))/(point->ordonnee+120)-10, 
			(15*(point->ordonnee)*(point->ordonnee))/(point->abscisse+120)-28); 
 	return p;
}

coordonnees f04Point(coordonnees point) { 

	coordonnees p = NULL;
	
 	p = construitPoint(
			point->ordonnee-10,
			point->abscisse-2); 
 	return p;
}

coordonnees f05Point(coordonnees point) {

	coordonnees p = NULL;
	int k = 200;

 	p = construitPoint(
			k*point->abscisse/(point->abscisse*point->abscisse+point->ordonnee*point->ordonnee)-0.0,
			k*point->ordonnee/(point->abscisse*point->abscisse+point->ordonnee*point->ordonnee)-0.0);
 	return p;
}

coordonnees f06Point(coordonnees point) { // cc -> -lm

	coordonnees p = NULL;
	float k = 1.25;

 	p = construitPoint(
			k*point->abscisse+8,
			k*point->ordonnee-6);
 	return p; 
}

coordonnees f07Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			0.5*point->ordonnee-10,
			0.005*point->abscisse*point->abscisse*point->abscisse);
 	return p;
}

coordonnees f08Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			point->abscisse+8,
			0.0065*point->ordonnee*point->ordonnee*point->ordonnee-22);
 	return p;
}

coordonnees f09Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			point->abscisse-1.2*point->ordonnee+25,
			0.075*point->ordonnee*point->ordonnee-10);
 	return p;
}

coordonnees f10Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			point->abscisse,
			80/(0.1+point->ordonnee)-3);
 	return p;
}

coordonnees f11Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			point->abscisse+14,
			-35/(0.1+point->ordonnee)+11.5);
 	return p;
}

coordonnees f12Point(coordonnees point) { 

	coordonnees p = NULL;

 	p = construitPoint(
			0.00800*(point->ordonnee*point->ordonnee-2*point->ordonnee+1)*point->abscisse+15,
			-90/point->ordonnee+14.5);
 	return p;
}
