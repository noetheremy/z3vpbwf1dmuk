/*
* Licence du code source de ce fichier : GNU General Public License v3.0 or later

*    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
*    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
*    Copyright (C) 2025  Jean-François Mai (alias jfm@)
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.

*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.

*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LIBPOINT_H
#define LIBPOINT_H

/* objet */
typedef struct _coordonnees * coordonnees;

/* constructeur */
coordonnees 	construitPoint(float, float);

/* destructeur */
void 		detruitPoint(coordonnees); 

coordonnees 	f00Point(coordonnees,
	       		float, float, float,
	       		float, float, float);	
coordonnees 	f01Point(coordonnees);
coordonnees 	f02Point(coordonnees);
coordonnees 	f03Point(coordonnees);
coordonnees 	f04Point(coordonnees);
coordonnees 	f05Point(coordonnees);
coordonnees 	f06Point(coordonnees);
coordonnees 	f07Point(coordonnees);
coordonnees 	f08Point(coordonnees);
coordonnees 	f09Point(coordonnees);
coordonnees 	f10Point(coordonnees);
coordonnees 	f11Point(coordonnees);
coordonnees 	f12Point(coordonnees);

#endif /* LIBPOINT_H */
