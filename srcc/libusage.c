/*
* Licence du code source de ce fichier : GNU General Public License v3.0 or later

*    Le logiciel libre perlromathsdr génère des dessins repérés au format pdf.
*    Pour ce faire, il "suffit" de lui fournir des coordonnées en entrée.
*    Copyright (C) 2025  Jean-François Mai (alias jfm@)
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.

*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.

*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "./srcc/libusage.h"

void 
usage(void) {
    	fprintf(stderr,
		"\nMerci d'utiliser le logiciel libre / open source perlromathsdr.\n\n"
		"Ce logiciel permet d'utiliser une application du plan affine "
	       	"(euclidien) muni d'un repère \n(orthonormé) sur lui-même qui envoie un "
		"point M de coordonnées (x ; y) en un point M' de \ncoordonnées (x' ; y'). "
		"Pour ce faire, vous avez le choix entre :\n\n"
		"(1) Soit d'utiliser l'application identité 'f01Point' via la commande :\n\n"
		"you$ ./perlromathsdr 'f01Point' 'Couleur des tracés via par un entier'\n\n"
		"(2) Soit d'utiliser une application qui est un endomorphisme "
		"'f00Point' via la commande :\n\n"
		"you$ ./perlromathsdr 'f00Point' 'Couleur des tracés via un nombre entier' "
        	"'a' 'b' 'c' 'd' 'u' 'v'\n\n"
		"en appliquant l'égalité matricielle suivante :\n\n"
        	"          / x' \\     / a b \\  / x \\     / u \\\n"
        	"         |      | = |       ||     | + |     |\n"
        	"          \\ y' /     \\ c d /  \\ y /     \\ v /\n\n"
		"(3) Soit d'autres applications 'f02Point', 'f03Point',... 'f12Point' "
	       	"(que vous pourrez, si \nvous le souhaitez, modifier en éditant le fichier ./srcc/libpoint.c), "
	       	"via la commande :\n\n"
		"you$ ./perlromathsdr 'fnPoint' 'Couleur des tracés via un nombre entier'\n\n"
	        "avec n = 02, 03,... , 12.\n\n"
		"Couleurs possibles :\n\n"
		"--------------------------------------------------------------------------\n"
		"1  -> BLACK | 2  -> RED      | 3  -> BLUE | 4  -> MAGENTA   | 5  -> VIOLET\n"
		"6  -> GREEN | 7  -> ORANGE   | 8  -> CYAN | 9  -> YELLOW    | 10 -> LIME\n"
		"11 -> BROWN | 12 -> DARKGRAY | 13 -> GRAY | 14 -> LIGHTGRAY | "
		"15 -> OLIVE\n16 -> PINK  | 17 -> PURPLE   | 18 -> TEAL\n"
		"--------------------------------------------------------------------------\n\n"
		);
}
